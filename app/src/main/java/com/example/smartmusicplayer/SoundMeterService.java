package com.example.smartmusicplayer;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class SoundMeterService extends IntentService {
    private SoundMeter Detector = null;
    private static final String ACTION_SOUNDMETER = "com.example.smartmusicplayer.action.SOUNDMETER";

    public SoundMeterService() {
        super("SoundMeterService");
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void startSoundMeter(Context context) {
        Intent intent = new Intent(context, SoundMeterService.class);
        intent.setAction(ACTION_SOUNDMETER);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_SOUNDMETER.equals(action)) {
                handleActionSoundMeter();
            }
        }
    }

    /**
     * Handle action in the provided background thread with the provided
     * parameters.
     */
    private void handleActionSoundMeter() {
        if (Detector == null) {
            Detector = new SoundMeter();
            Detector.start();
        }
        new Thread(new Runnable(){
            public void run() {
                while(true)
                {
                    double volume = 0;
                    LocalBroadcastManager localBroadcastManager;
                    localBroadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());
                    try {
                        Thread.sleep(500);
                        volume = Detector.getAmplitude();
                        Log.w("SamuelSoundMeterService", String.valueOf(volume));
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    if(volume >= 3000.0) {
                        Log.w("SamuelSoundMeterService", "PAUSE");
                        Intent broadcastIntent = new Intent();
                        broadcastIntent.setAction(MainActivity.RECEIVE_PAUSE);
                        localBroadcastManager.sendBroadcast(broadcastIntent);

                    } else {
                        Log.w("SamuelSoundMeterService", "PLAY");
                        Intent broadcastIntent = new Intent();
                        //broadcastIntent.setAction(MainActivity.RECEIVE_PLAY);
                        localBroadcastManager.sendBroadcast(broadcastIntent);
                    }
                }
            }
        }).start();
    }
}