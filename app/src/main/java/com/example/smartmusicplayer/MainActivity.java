
package com.example.smartmusicplayer;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.widget.Button;
import android.media.MediaPlayer;
import android.view.View;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ToggleButton;
//import android.media.MediaRecorder;

public class MainActivity extends AppCompatActivity {
    public static final String RECEIVE_PAUSE = "com.example.smartmusicplayer.RECEIVE_PAUSE";
    public static final String RECEIVE_PLAY = "com.example.smartmusicplayer.RECEIVE_PLAY";
    private BroadcastReceiver bReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final ToggleButton Play = (ToggleButton) findViewById(R.id.Play);
        // Button Pause = (Button) findViewById(R.id.Play);
        final MediaPlayer mp = MediaPlayer.create(MainActivity.this, R.raw.youareanidiot);
        mp.setLooping(true);
        SoundMeterService.startSoundMeter(getApplicationContext());

        Play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Play.isChecked()) {
                    Log.w("PlayOnClick", "Checked");
                    mp.start();
                } else {
                    Log.w("PlayOnClick", "Not Checked");
                    mp.pause();
                }
            }
        });

        bReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.w("SamuelSoundMeterService", "RECEIVED");
                if (intent.getAction().equals(RECEIVE_PAUSE)) {
                    Play.setChecked(false);
                    mp.pause();
                }
                if(intent.getAction().equals(RECEIVE_PLAY)) {
                    Play.setChecked(true);
                    mp.start();
                    Log.w("SamuelSoundMeterService", "PLAY_RECEIVED");
                }
            }
        };

        LocalBroadcastManager bManager = LocalBroadcastManager.getInstance(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(RECEIVE_PAUSE);
        intentFilter.addAction(RECEIVE_PLAY);
        bManager.registerReceiver(bReceiver, intentFilter);

    }
    @Override
    protected void onResume() {
        super.onResume();
    }
}
